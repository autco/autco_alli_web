$(document).ready(function(){
    var fits = {};

    //Thrasher
    fits['start-thrasher']='';
    fits['thrasher']='16242:519;1:2048;1:5971;1:31926;1:448;1:2889;7:4477;1:31668;1:31722;1:31758;1:12608;3340:12625;1500:21898;2500:28668;50::';
    
    function osmiumRedirect(fitId) {
        window.open('https://o.smium.org/new/dna/' + fits[fitId], '_blank');
    }


    //Проверяем какой фит щёлкнули, и вызываем функцию getFit с нужным фитом
    $("a.fits").click(function() {
        fidId = $(this).attr( "id" );
        osmiumRedirect(fidId);
        return false;
    });
	
});