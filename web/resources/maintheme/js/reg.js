function check_data_and_reg() {
    //считать параметры
    var usermail = $("#regEmail").val();	
    var userpass = $("#regPassword").val();
    var userpass2 = $("#regPassword2").val();
    //сравнить пароли
    var pass_equal = userpass.localeCompare(userpass2);

    var no_errors = true;
    var errmsg = "";
    //проверить длину пароля
    if (userpass.length < 8) {
        errmsg += "Ошибка: длина пароля должна быть не менее 8 символов!\n";
        no_errors = false;
    }
    //сообщить о несовпадении паролей
    if (pass_equal !== 0) {
        no_errors = false;
        errmsg += "Ошибка: пароли не совпадают!\n";
    }
    //если есть ошибки, покрасить текст в красный, задать размеры
    if (no_errors !== true) {
        alert(errmsg);
        return false;
    }
    else {
        //иначе вывести окно успешной регистрации
        /*$.post("/processRegister", { email: usermail, password: userpass },
        function(data){ 
            // пока что просто алерт
            alert(data);
        });*/
        return true;

    }
}