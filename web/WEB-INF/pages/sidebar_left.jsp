<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script>
$(document).ready(function(){
    $("li.drop-0").click(function(){
        $("ul.drop-0").toggle();
    });

    $("li.drop-1").click(function(){
        $("ul.drop-1").toggle();
    });
    
    $("li.drop-2").click(function(){
        $("ul.drop-2").toggle();
    });
    
    $("li.drop-3").click(function(){
        $("ul.drop-3").toggle();
    });
});
</script>

<ul class="topnav" id="topNav">
    <c:if test="${userSSO.hasUserCred()}">
        <spring:url value="/profile/${userSSO.userCred.id}" var="profileurl" htmlEscape="true"/>
        <li><a href="${profileurl}">${userSSO.userCred.email}</a></li>
        <li><a href="/processLogout">Выйти</a></li>
        <li><a href="/index.htm">Главная</a></li>
        <!--<li><a href="#">Code: ${message}</a></li>-->
        <li><a href="/news.htm">Новости</a></li>
        <li><a href="/events.htm">События</a></li>
        <li><a href="/regular.htm">Правила и требования</a></li>
        <li class="drop-0"><a href="#">Фитинг</a></li>
            <ul class="drop-0">
                <li class="drop-1"><a href="#">Боевой отдел</a></li>
                <ul class="drop-1">
                    <li><a href="/wardepartment/dps.htm">Дамагер</a></li>
                    <li><a href="/wardepartment/ceptor.htm">Скаут</a></li>
                    <li><a href="/wardepartment/logi.htm">Логист</a></li>
                    <li><a href="/wardepartment/recon.htm">Электроника</a></li>
                    <li><a href="/wardepartment/command.htm">Бонусник</a></li>
                    <li><a href="/wardepartment/atm.htm">Антимелочь</a></li>
                    <li><a href="/wardepartment/dictor.htm">Диктор</a></li>
                </ul>
                <li class="drop-2"><a href="#">Производственный отдел</a></li>
                <ul class="drop-2">
                    <li><a href="#">Майнер</a></li>
                    <li><a href="#">Производство</a></li>
                    <li><a href="#">Планетарка</a></li>
                    <li><a href="#">Сканирование</a></li>
                    <li><a href="#">Ледово/газовая экспедиция</a></li>
                </ul>
                <li class="drop-3"><a href="#">Дополнительно</a></li>
                <ul class="drop-3">
                    <li><a href="#">Топ PVE</a></li>
                    <li><a href="#">Solo-PVP</a></li>
                </ul>
                
            </ul>
        <li><a href="/contacts.htm">Контакты</a></li>
    </c:if>
    
    <c:if test="${!userSSO.hasUserCred()}">
    <li><a href="/index.htm">Главная</a></li>
    <!--<li><a href="#">Code: ${message}</a></li>-->
    <li><a href="/register.htm">Регистрация/вход</a></li>
    <li><a href="/contacts.htm">Контакты</a></li>
    </c:if>
</ul>