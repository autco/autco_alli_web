<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Сайт альянса AUTISM COLLECTORS из игры EvE online">
    <meta name="Keywords" content="eve online, EVE, ева онлайн,">
    <title>AUTISM COLLECTORS</title>
    <%@include file="template/header.jsp" %>  
</head>
<body>
<div id="grid" class="grid">
    <div class="grid__item">
        <%@include file="sidebar_left.jsp" %>
    </div>
    <div class="grid__item">
        <div class="content">
        <div class="logo"><img src="/img/logo.jpg" width="200" height="200" alt="logo">AUTISM COLLECTORS(тут типа на всю строку картинкой будет логотип вместе с надписью, которая прям в картинке будет прописана)</div>
        <h1 style="text-align: center;"><span style="color: lightgray;">Наш офицерский состав</span></h1>
<p style="text-align: center;">СЕО сообщества (Chief Executive Officer) - <span style="text-align: center; color: LightSteelBlue !important;">Amaretto Huren</span> 
<img class="size-medium wp-image-240 aligncenter" src="http://193.124.178.147/wp-content/uploads/2016/07/ss2016-02-03at07.10.50.jpg" alt="ss+(2016-02-03+at+07.10.50)" width="300" height="300" /></p>
<p style="text-align: justify;">В обязанности СЕО входит контроль за текущей ситуацией сообщества, определение вектора развития оного, формирование четких целей и задач для каждого подразделения сообщества. СЕО имеет решающий голос при принятии решений, которые могут отразиться на стратегическом положении всего содружества. Также СЕО принимает политические решения, отражающие взаимодействие сообщества с другими объединениями пилотов.</p>

<h2 style="text-align: center;"><span style="color: lightgray;">Главы отделов</span></h2>
<p style="text-align: justify;">Это люди, которые были выбраны и поставлены на данные посты за свои заслуги и достижения в определенных областях игры или личных качеств.</p>
<p style="text-align: center;">- финансового отдела (Head of Financial Department) <span style="text-align: center; color: LightSteelBlue !important;">Timeruf</span>
<img class="size-medium wp-image-243 aligncenter" src="http://193.124.178.147/wp-content/uploads/2016/07/ss2016-04-27at04.12.55.jpg" alt="ss+(2016-04-27+at+04.12.55)" width="300" height="300" /></p>
<p style="text-align: justify;">В обязанности главы финансового отдела входит мониторинг и контроль всех финансовых потоков и операций, протекающих внутри сообщества. Также в его обязанности входит предоставление и распределение финансовых средств по членам содружества и основным задачам, которые поставлены руководством альянса на данный момент.</p> 

<p style="text-align: center;">- отдела разработок (Head of Development Department) <span style="text-align: center; color: LightSteelBlue !important;">Eotain Tornvau</span>
<img class="size-medium wp-image-241 aligncenter" src="http://193.124.178.147/wp-content/uploads/2016/07/ss2016-04-27at04.11.35.jpg" alt="ss+(2016-04-27+at+04.11.35)" width="300" height="298" /></p>
<p style="text-align: justify;">В обязанности гор входит анализ текущего состояния оборудования и разработка оснастки кораблей, исходя из требуемых задач и имеющихся компонентов. Также в его обязанности входит проверка оснастки действующих кораблей флота сообщеста.</p>

<p style="text-align: center;">- межнационального отдела (Head of International Department) <span style="text-align: center; color: LightSteelBlue !important;">XIII Randomize</span>
<img src="http://193.124.178.147/wp-content/uploads/2016/07/ss2016-04-27at04.13.55-300x300.jpg" alt="ss+(2016-04-27+at+04.13.55)" width="300" height="300" class="aligncenter size-medium wp-image-113" /></p>
<p style="text-align: justify;">XIII Randomize отвечает за нормальное взаимодействие всех членов сообщества между собой и решает межнациональные вопросы внутри сообщества. Осуществляет нормализацию взаимоотношений между офицерским составом и интернациональными членами сообщества.</p>

<p style="text-align: center;">- производственного отдела (Head of Production Department) <span style="text-align: center; color: LightSteelBlue !important;">---> место вакантно <---</span></p> 
<p style="text-align: justify;">Отвечает за всю процедуру добычи полезных ископаемых, проводимых данной корпорацией. Также в обязанности главы производственного отдела входит налаживание и обеспечение производственного процесса всего необходимого оборудования для данной корпорации и организация маршрутов ее доставки.</p>



<p style="text-align: center;">- боевого отдела (Head of Combat Department) <span style="text-align: center; color: LightSteelBlue !important;">DarthKoron</span>
<img src="http://193.124.178.147/wp-content/uploads/2016/07/DarthKoron-300x300.png" alt="darthkoron" width="300" height="300" class="aligncenter size-medium wp-image-1401" /></p>
<p style="text-align: justify;">В обязанности главы боевого отдела входит стратегическое планирование всех проводимых боевых операций. Также данный человек отвечает за структуру и содержание тренировочного процесса членов корпорации. </p>



<p style="text-align: center;">- отдела безопасности (Head of Security Department) <span style="text-align: center; color: LightSteelBlue !important;">Dart Sion</span>
<img src="http://193.124.178.147/wp-content/uploads/2016/10/Dart-Sion-300x300.png" alt="dart-sion" width="300" height="300" class="aligncenter size-medium wp-image-1310" /></p>
<p style="text-align: justify;">Отвечает за поддержание целостности и сохранности информации о составе сообщества, о его структуре, о ведущихся разработках и планах. Данный человек должен пресекать: попытки дестабилизации структуры сообщества, действия, приводящие к расколу сообщества, утечки информации и проводить отслеживание взаимодействия кадров с конкурирующими сообществами. </p>

        </div>    
    </div>
</div>
</body>
</html>