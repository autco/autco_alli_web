<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Сайт альянса AUTISM COLLECTORS из игры EvE online">
    <meta name="Keywords" content="eve online, EVE, ева онлайн,">
    <title>AUTISM COLLECTORS</title>
    <%@include file="template/header.jsp" %>
    <style>
    .reg-fade-in {
	opacity: 1;
	animation-name: regFadeInOpacity;
	animation-iteration-count: 1;
	animation-timing-function: ease-in;
	animation-duration: 0.5s;
    }

    @keyframes regFadeInOpacity {
            0% {
                    opacity: 0;
            }
            100% {
                    opacity: 1;
            }
    }
    div.h2 {
        text-align: center;
        width: 50%;
        min-width: 50%;
        display: inline-block;
        margin-top: 20px;
        margin-bottom: 40px;
        font-weight: bold;
        font-size: 20px;
    }
    div.reg-active {
        background-color: lightblue;
        color: black;
    }
    div.reg-inactive {
        background-color: lightgray;
        color: black;
        opacity: 0.5;
    }
    div.reg-inactive:hover {
        background-color: lightblue;
        color: black;
        opacity: 0.7;
    }
    input[type=text], input[type=password] {
        border: none;
        background: none;
        border-bottom: 2px solid lightblue;
        color: rgb(186, 186, 186);
    }
    input[type=text]:focus, input[type=password]:focus {
        background-color: lightblue;
        color: black;
    }
    button {
        background-color: lightskyblue;
        color: black;
        transition-duration: 0.2s;
        border: 2px solid lightblue;
        padding: 5px 24px;
        font-size: 18px;
        margin-top: 10px;
    }
    button:hover {
        background-color: lightblue;
        color: white;
    }
    </style>
    <script>
        $(document).ready(function(){
            $("#authToggle").click(function(){
                $("#authFrame").toggle();
                $("#regFrame").toggle();
            });
            $("#regToggle").click(function(){
                $("#authFrame").toggle();
                $("#regFrame").toggle();
            });
            $("#regFrame").toggle();
        });
    </script>
</head>
<body>
<div id="grid" class="grid">
    <div class="grid__item">
        <!--<form action="index" method="GET">
            <input type="image" name="authorize" src="https://images.contentful.com/idjq7aai9ylm/4fSjj56uD6CYwYyus4KmES/4f6385c91e6de56274d99496e6adebab/EVE_SSO_Login_Buttons_Large_Black.png?w=270&h=45" alt="Submit">
        </form>-->     
        <%@include file="sidebar_left.jsp" %>
    </div>
    <div class="grid__item">
        <div class="content">
            <div id="authFrame" class="reg-fade-in" style="width: 100%;">
                <div class="h2 reg-active">Вход</div><div id="authToggle" class="h2 reg-inactive">Регистрация</div>
                <form:form id="authForm" modelAttribute="authUserCred" action="processAuthorize" method="post" style="width: 100%;">
                    <table align="center">
                        <tr>
                            <td>
                                <form:label path="email">Email</form:label>
                            </td>
                            <td>
                                <form:input path="email" name="email" id="email" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <form:label path="password">Пароль</form:label>
                            </td>
                            <td>
                                <form:password path="password" name="password" id="password" />
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <td>
                                <form:button id="login" name="login">Войти</form:button>
                            </td>
                        </tr>
                        <tr></tr>

                    </table>

                </form:form>
            </div>
            <div id="regFrame" class="reg-fade-in" style="width: 100%;">
                <div class="h2 reg-active">Регистрация</div><div id="regToggle" class="h2 reg-inactive">Вход</div>
                <form:form id="regForm" modelAttribute="regUserCred" action="processRegister" method="post" style="width: 100%">
                    <table align="center">
                        <tr>
                            <td>
                                <form:label path="email">Email</form:label>
                            </td>
                            <td>
                                <form:input path="email" name="regEmail" id="email" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <form:label path="password">Пароль</form:label>
                            </td>
                            <td>
                                <form:password path="password" name="regPassword" id="regPassword" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <form:label path="password2">Повторите пароль</form:label>
                            </td>
                            <td>
                                <form:password path="password2" name="regPassword2" id="regPassword2" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                ${regErrorMsg}
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <td>
                                <form:button id="register" name="register">Войти</form:button>
                            </td>
                        </tr>
                        <tr></tr>

                    </table>

                </form:form>
            </div>
        </div> 
    </div>
</div>
  
</body>
</html>