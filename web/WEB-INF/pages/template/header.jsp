<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

  
<spring:url value="/resources/img/icon.jpg" var="siteicon"/>
<spring:url value="/resources/img/logo.jpg" var="pic_LogoAlli"/>

<link rel="stylesheet" type="text/css" href="/resources/css/main.css"/>
<link rel="icon" href="${siteicon}"/>
<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script> 