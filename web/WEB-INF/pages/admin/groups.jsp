<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Сайт альянса AUTISM COLLECTORS из игры EvE online">
    <meta name="Keywords" content="eve online, EVE, ева онлайн,">
    <title>AUTISM COLLECTORS</title>
    <%@include file="../template/header.jsp" %>  
</head>
<body>
<div id="grid" class="grid">
    <div class="grid__item">
        <%@include file="../sidebar_left.jsp" %>
    </div>
    <div class="grid__item">
        <div class="content" style="color: rgb(186, 186, 186)">
            <h2 style="width: 100%">Список администраторов</h2>
            <table border="2" width="70%" cellpadding="2">  
            <tr><th>Id</th><th>Почта</th><th>Действия</th></tr>  
            <c:forEach var="item" items="${adminsList}">   
            <tr>  
            <td>${item.id}</td>  
            <td>${item.email}</td>  
            <c:if test = "${userSSO.userCred.id != item.id}">
                <spring:url value="/admin/groups/removeFromSiteadmins/${item.id}" var="remfromadminsurl" htmlEscape="true"/>
                <td><div class="a-like-button"><a href="${remfromadminsurl}">Убрать из группы</a></div></td>  
            </c:if>
            </tr>  
            </c:forEach>  
            </table>
            <h2 style="width: 100%">Список CEO</h2>
            <table border="2" width="70%" cellpadding="2">  
            <tr><th>Id</th><th>Почта</th><th>Действия</th></tr>  
            <c:forEach var="item" items="${ceosList}">   
            <tr>  
            <td>${item.id}</td>  
            <td>${item.email}</td>  
            <c:if test = "${userSSO.userCred.id != item.id}">
                <spring:url value="/admin/groups/removeFromCEOs/${item.id}" var="remfromceosurl" htmlEscape="true"/>
                <td><div class="a-like-button"><a href="${remfromceosurl}">Убрать из группы</a></div></td> 
            </c:if>
            </tr>  
            </c:forEach>  
            </table>
            <h2 style="width: 100%">Список офицеров</h2>
            <table border="2" width="70%" cellpadding="2">  
            <tr><th>Id</th><th>Почта</th><th>Действия</th></tr>  
            <c:forEach var="item" items="${offcsList}">   
            <tr>  
            <td>${item.id}</td>  
            <td>${item.email}</td>   
            <c:if test = "${userSSO.userCred.id != item.id}">
                <spring:url value="/admin/groups/removeFromOfficers/${item.id}" var="remfromoffcsurl" htmlEscape="true"/>
                <td><div class="a-like-button"><a href="${remfromoffcsurl}">Убрать из группы</a></div></td>  
            </c:if>
            </tr>  
            </c:forEach>  
            </table>
            <h2 style="width: 100%">Список Флиткомов</h2>
            <table border="2" width="70%" cellpadding="2">  
            <tr><th>Id</th><th>Почта</th><th>Действия</th></tr>  
            <c:forEach var="item" items="${fcsList}">   
            <tr>  
            <td>${item.id}</td>  
            <td>${item.email}</td>
            <c:if test = "${userSSO.userCred.id != item.id}">
                <spring:url value="/admin/groups/removeFromFleetCommanders/${item.id}" var="remfromfcssurl" htmlEscape="true"/>
                <td><div class="a-like-button"><a href="${remfromfcssurl}">Убрать из группы</a></div></td>
            </c:if>
            </tr>  
            </c:forEach>  
            </table>
        </div>  
    </div>
    <div class="grid__item">
        <%@include file="../sidebar_right.jsp" %>
    </div>
</div>
  
</body>
</html>