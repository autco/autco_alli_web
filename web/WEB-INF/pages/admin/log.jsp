<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Сайт альянса AUTISM COLLECTORS из игры EvE online">
    <meta name="Keywords" content="eve online, EVE, ева онлайн,">
    <title>AUTISM COLLECTORS</title>
    <%@include file="../template/header.jsp" %>  
</head>
<body>
<div id="grid" class="grid">
    <div class="grid__item">
        <%@include file="../sidebar_left.jsp" %>
    </div>
    <div class="grid__item">
        <div class="content" style="color: rgb(186, 186, 186)">
            <h2 style="width: 100%">Журнал событий</h2>
            <table border="2" width="70%" cellpadding="2">  
            <tr><th>Id</th><th>Пользователь</th><th>Действие</th><th>Время</th></tr>  
            <c:forEach var="item" items="${logsList}">   
                <tr>  
                    <td>${item.id}</td>  
                    <td>${item.userid.email}</td>  
                    <td>${item.logaction}</td> 
                    <td>${item.logtime}</td>
                </tr>  
            </c:forEach>  
            </table>  
        </div>  
    </div>
    <div class="grid__item">
        <%@include file="../sidebar_right.jsp" %>
    </div>
</div>
  
</body>
</html>