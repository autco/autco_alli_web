<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Сайт альянса AUTISM COLLECTORS из игры EvE online">
    <meta name="Keywords" content="eve online, EVE, ева онлайн,">
    <title>AUTISM COLLECTORS</title>
    <%@include file="../template/header.jsp" %>  
</head>
<body>
<div id="grid" class="grid">
    <div class="grid__item">
        <%@include file="../sidebar_left.jsp" %>
    </div>
    <div class="grid__item">
        <div class="content" style="color: rgb(186, 186, 186)">
            <h2 style="width: 100%">Поиск</h2>
            <form:form id="searchForm" action="users/userSearch" modelAttribute="searchUserCred"  method="post">
                <form:label path="email">Email</form:label>
                <form:input path="email" name="email" id="email"/>
                <form:button id="search" name="search">Найти</form:button>
            </form:form>
            <h2 style="width: 100%">Список пользователей</h2>
            <table border="2" width="70%" cellpadding="2">  
            <tr><th>Id</th><th>Почта</th><th>Пароль</th><th>Статус</th><th colspan="2">Действия</th></tr>  
            <c:forEach var="item" items="${usersList}">   
            <tr>  
            <td>${item.id}</td>  
            <td>${item.email}</td>  
            <td>${item.password}</td> 
            <c:choose>
                <c:when test = "${item.approved == 1}">
                    <td>
                    Подтвержден
                    </td>
                </c:when>
                <c:otherwise>
                    <spring:url value="/admin/users/approveUser/${item.id}" var="aprusrurl" htmlEscape="true"/>
                    <td><div class="a-like-button"><a href="${aprusrurl}">Подтвердить</a></div></td>
                </c:otherwise>
            </c:choose>      
            <td>
                <c:if test = "${userSSO.userCred.haveCEORole() || userSSO.userCred.haveSiteadminRole()}">
                <spring:url value="/profile/${item.id}" var="profileurl" htmlEscape="true"/>
                <div class="a-like-button"><a href="${profileurl}">Профиль</a></div>
                </c:if>       
            </td>
            <td>
                <c:if test = "${item.id != userSSO.userCred.id}">
                <spring:url value="/admin/users/deleteUser/${item.id}" var="delusrurl" htmlEscape="true"/>
                <div class="a-like-button"><a href="${delusrurl}">Удалить</a></div>
                </c:if>
            </td>  
            </tr>  
            </c:forEach>  
            </table>  
        </div>  
    </div>
    <div class="grid__item">
        <%@include file="../sidebar_right.jsp" %>
    </div>
</div>
  
</body>
</html>