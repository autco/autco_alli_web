<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Сайт альянса AUTISM COLLECTORS из игры EvE online">
    <meta name="Keywords" content="eve online, EVE, ева онлайн,">
    <title>AUTISM COLLECTORS</title>
    <%@include file="template/header.jsp" %>
</head>
<body>
<div id="grid" class="grid">
    <div class="grid__item">
        <%@include file="sidebar_left.jsp" %>
    </div>
    <div class="grid__item">
        <div class="content" style="color: rgb(186, 186, 186); text-align: center;">
            <c:if test = "${userSSO.userCred.haveCEORole() && (isEventAdding == null || isEventAdding == false)}">
                <div style='width: 100%'>
                <spring:url value="/events/addEvent" var="addevturl" htmlEscape="true"/>
                <div class="a-like-button"><a href="${addevturl}">Добавить событие</a></div>
                </div>
            </c:if>
            <c:if test = "${userSSO.hasUserCred() && (isEventAdding == null || isEventAdding == false)}">
                <h2 style='width: 100%'>Запланированные события</h2>
                <c:if test = "${evtList != null}">
                <table border="2" width="70%" cellpadding="2"> 
                    <spring:url value="/events?sort=${dateSort}" var="sortevturl" htmlEscape="true"/>
                    <tr><th><div class="a-like-button"><a href="${sortevturl}">Дата</a></div></th><th>Название</th><th>Описание</th><th>Действия</th></tr>  
                    <c:forEach var="item" items="${evtList}">   
                    <tr>  
                        <td>${item.eventdate}</td>  
                        <td>${item.title}</td>
                        <td>${item.description}</td>
                        <c:if test = "${!userSSO.userCred.haveSubscriptionToEvent(item.id)}">
                            <spring:url value="/events/subscribe/${item.id}" var="subscrurl" htmlEscape="true"/>
                            <td><div class="a-like-button"><a href="${subscrurl}">Подписаться</a></div></td>
                        </c:if>
                        <c:if test = "${userSSO.userCred.haveSubscriptionToEvent(item.id)}">
                            <spring:url value="/events/unsubscribe/${item.id}" var="unsubscrurl" htmlEscape="true"/>
                            <td><div class="a-like-button"><a href="${unsubscrurl}">Отписаться</a></div></td>
                        </c:if>
                        <c:if test = "${userSSO.userCred.haveCEORole() || userSSO.userCred.haveSiteadminRole()}">
                            <spring:url value="/eventinfo/${item.id}" var="evtinfourl" htmlEscape="true"/>
                            <td><div class="a-like-button"><a href="${evtinfourl}">Подробно</a></div></td>
                        </c:if>
                    </tr>  
                    </c:forEach>  
                </table>
                </c:if>
            </c:if>
            <c:if test = "${userSSO.hasUserCred() && userSSO.userCred.haveCEORole() && isEventAdding == true}">
                <h2 style="width: 100%"> Добавление события </h2>
                <form:form id="evtAddForm" modelAttribute="calendarEvent" action="confirmAdding" method="post" style="width: 100%">
                    <table align="center">
                        <tr>
                            <td>
                                <form:label path="eventdate">Дата</form:label>
                            </td>
                            <td>
                                <form:input path="eventdate" name="eventdate" id="eventdate" type="date" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <form:label path="title">Название</form:label>
                            </td>
                            <td>
                                <form:input path="title" name="title" id="title" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <form:label path="description">Описание</form:label>
                            </td>
                            <td>
                                <form:textarea path="description" rows="5" cols="30" name="description" id="description" />
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <td>
                                <form:button id="addEvt" name="addEvt">Добавить</form:button>
                            </td>
                        </tr>
                        <tr></tr>

                    </table>

                </form:form>
            </c:if>
        </div>
    </div>
    <div class="grid__item">
        <%@include file="sidebar_right.jsp" %>
    </div>
</div>
  
</body>
</html>