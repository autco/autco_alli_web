<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<ul class="topnav">
    <c:if test="${userSSO.hasUserCred() && hasSiteAdminRole}">
    <spring:url value="/admin/users.htm" var="usersurl" htmlEscape="true"/>
    <spring:url value="/admin/groups.htm" var="groupsurl" htmlEscape="true"/>
    <spring:url value="/admin/log.htm" var="logurl" htmlEscape="true"/>
    <li><a href="#">Меню администратора</a></li>
    <ul style="display: block; list-style: none;">
        <li><a href="${usersurl}">Пользователи</a></li>
        <li><a href="${groupsurl}">Группы</a></li>
        <li><a href="${logurl}">Журнал событий</a></li>
    </ul>
    </c:if>
</ul>