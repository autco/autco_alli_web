<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Сайт альянса AUTISM COLLECTORS из игры EvE online">
    <meta name="Keywords" content="eve online, EVE, ева онлайн,">
    <title>AUTISM COLLECTORS</title>
    <%@include file="template/header.jsp" %> 
</head>
<body>
<div id="grid" class="grid">
    <div class="grid__item">
        <%@include file="sidebar_left.jsp" %>
    </div>
    <div class="grid__item">
        <div class="content" style="color: rgb(186, 186, 186); text-align: center;">
            <c:if test = "${userCred != null}">
            <h2 style="width: 100%">Профиль ${userCred.email} [${userCred.id}]</h2>
            <p style="width: 100%">Почта - ${userCred.email}</p>
            <p style="width: 100%">Пароль - ${userCred.password}</p>
            <c:choose>
                <c:when test = "${userCred.approved == 1}">
                    <p style="width: 100%">Подтвержден - Да</p>
                </c:when>
                <c:otherwise>
                    <p style="width: 100%">Подтвержден - Нет</p>
                </c:otherwise>
            </c:choose>
            <p style="width: 100%">Группы:</p>
            <table border='2' align='center'>
                <c:if test = "${userCred.haveSiteadminRole()}"><tr><td>Администратор</td></tr></c:if>
                <c:if test = "${userCred.haveCEORole()}"><tr><td>Исполнительный директор корпорации</td></tr></c:if>
                <c:if test = "${userCred.haveFleetCommanderRole()}"><tr><td>Командир флота</td></tr></c:if>
                <c:if test = "${userCred.haveOfficerRole()}"><tr><td>Офицер</td></tr></c:if>
                <c:if test = "${userSSO.userCred.haveSiteadminRole()}">
                    <spring:url value="/profile/addToSiteadmins/${userCred.id}" var="addtoadmurl" htmlEscape="true"/>
                    <spring:url value="/profile/addToCeos/${userCred.id}" var="addtoceourl" htmlEscape="true"/>
                    <spring:url value="/profile/addToOfficers/${userCred.id}" var="addtooffcurl" htmlEscape="true"/>
                    <spring:url value="/profile/addToFleetCommanders/${userCred.id}" var="addtofcurl" htmlEscape="true"/>
                    <c:if test = "${!userCred.haveSiteadminRole()}"><tr><td><div class="a-like-button"><a href="${addtoadmurl}">Добавить в администраторы</a></div></td></tr></c:if>
                    <c:if test = "${!userCred.haveCEORole()}"><tr><td><div class="a-like-button"><a href="${addtoceourl}">Добавить в CEO</a></div></td></tr></c:if>
                    <c:if test = "${!userCred.haveFleetCommanderRole()}"><tr><td><div class="a-like-button"><a href="${addtofcurl}">Добавить в флиткомы</a></div></td></tr></c:if>
                    <c:if test = "${!userCred.haveOfficerRole()}"><tr><td><div class="a-like-button"><a href="${addtooffcurl}">Добавить в офицеров</a></div></td></tr></c:if>
                </c:if>
            </table>
            </c:if>
        
            <c:if test = "${userCred == null}">
                Ошибка открытия профиля!
            </c:if>
        </div>  
    </div>
    <div class="grid__item">
        <%@include file="sidebar_right.jsp" %>
    </div>
</div>
  
</body>
</html>