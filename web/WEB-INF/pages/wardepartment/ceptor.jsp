<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Сайт альянса AUTISM COLLECTORS из игры EvE online">
    <meta name="Keywords" content="eve online, EVE, ева онлайн,">
    <title>AUTISM COLLECTORS</title>
    <%@include file="../template/header.jsp" %>
    <script type="text/javascript" src="/resources/js/fittings.js"></script>
    <script>
        $(document).ready(function(){
            $("ul.drop-0").toggle();
            $("ul.drop-1").toggle();
            var pageId = window.location.pathname.substring(0, window.location.pathname.lastIndexOf('.'));
            $('a[href*="' + pageId + '"]').addClass('active');
        });
    </script>
</head>
<body>
<div id="grid" class="grid">
    <div class="grid__item">
        <%@include file="../sidebar_left.jsp" %>
    </div>
    <div class="grid__item">
        <div class="content">
            <div class="logo"><img src="/img/logo.jpg" width="200" height="200" alt="logo">AUTISM COLLECTORS(тут типа на всю строку картинкой будет логотип вместе с надписью, которая прям в картинке будет прописана)</div>

            <div class="zag"><h1>low-skill</h1></div>
            <div class="box">
                <img src="/resources/img/Crow.jpg" alt="Crow" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Ares.jpg" alt="Ares" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/no_image.png" alt="malediktion" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Stiletto.png" alt="Stiletto" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Garmur.jpg" alt="Garmur" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Keres.jpg" alt="Keres" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div> 
            <div class="zag"><h1>mid-skill</h1></div>
            <div class="box">
                <img src="/resources/img/Eris.jpg" alt="Eris" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Flycatcher.png" alt="Flycatcher" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Heretic.png" alt="Heretic" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Sabre.png" alt="Sabre" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="zag"><h1>high-skill</h1></div>
            <div class="box">
                <img src="/resources/img/Broadsword.png" alt="Broadsword" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Devoter.jpg" alt="Devoter" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Orthrus.png" alt="Orthrus" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Onyx.png" alt="Onyx" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Phobos.jpg" alt="Phobos" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Lachesis.jpg" alt="Lachesis" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>           
        </div>    
    </div>
</div>
</body>
</html>