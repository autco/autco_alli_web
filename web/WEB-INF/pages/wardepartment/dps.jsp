<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Сайт альянса AUTISM COLLECTORS из игры EvE online">
    <meta name="Keywords" content="eve online, EVE, ева онлайн,">
    <title>AUTISM COLLECTORS</title>
    <%@include file="../template/header.jsp" %>
    <script type="text/javascript" src="/resources/js/fittings.js"></script>
    <script>
        $(document).ready(function(){
            $("ul.drop-0").toggle();
            $("ul.drop-1").toggle();
            var pageId = window.location.pathname.substring(0, window.location.pathname.lastIndexOf('.'));
            $('a[href*="' + pageId + '"]').addClass('active');
        });
    </script>
</head>
<body>
<div id="grid" class="grid">
    <div class="grid__item">
        <%@include file="../sidebar_left.jsp" %>
    </div>
    <div class="grid__item">
        <div class="content">
            <div class="logo"><img src="/img/logo.jpg" width="200" height="200" alt="logo">AUTISM COLLECTORS(тут типа на всю строку картинкой будет логотип вместе с надписью, которая прям в картинке будет прописана)</div>

            <div class="zag"><h1>low-skill</h1></div>
            <div class="box">
                <img src="/resources/img/Thrasher.jpg" alt="Thrasher" width="100%">
                <p><a id="start-thrasher" class="fits" href="#">Стартовый</p>
                <p><a id="thrasher" class="fits" href="#">Конечный</a></p>
                <p>Рекомендуемые импланты</p>
                <p>Проверка скилов под данный корабль</p>
            </div>
            <div class="box">
                <img src="/resources/img/Catalyst.png" alt="Catalyst" width="100%">
                <p>Альфа-аккаунт</p>
                <p>Недокаченый</p>
                <p>Конечный</p>
                <p>Рекомендуемые импланты</p>
                <p>Проверка скилов под данный корабль</p>
            </div>
            <div class="box">
                <img src="/resources/img/Svipul.jpg" alt="Svipul" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Проверка скилов под данный корабль</p>
            </div>
            <div class="box">
                <img src="/resources/img/Hecate.png" alt="Hecate" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Проверка скилов под данный корабль</p>
            </div>

            <div class="zag"><h1>mid-skill</h1></div>
            <div class="box">
                <img src="/resources/img/Sacrilege.jpg" alt="Sacrilege" width="100%">
                <p>клозовая</p>
                <p>лонговая</p>
                <p>Рекомендуемые импланты</p>
                <p>Проверка скилов под данный корабль</p>
            </div>
            <div class="box">
                <img src="/resources/img/Gila.png" alt="Gila" width="100%">
                <p>Альфа-аккаунт</p>
                <p>Недокаченый</p>
                <p>Конечный</p>
                <p>Рекомендуемые импланты</p>
                <p>Проверка скилов под данный корабль</p>
            </div>
            <div class="box">
                <img src="/resources/img/Armageddon.jpg" alt="Armageddon" width="100%">
                <p>Альфа-аккаунт</p>
                <p>дроновая</p>
                <p>ракетная</p>
                <p>Рекомендуемые импланты</p>
                <p>Проверка скилов под данный корабль</p>
            </div>
            <div class="box">
                <img src="/resources/img/Typhoon.jpg" alt="Typhoon" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
                <p>Проверка скилов под данный корабль</p>
            </div>
            <div class="zag"><h1>high-skill</h1></div>
            <div class="box">
                <img src="/resources/img/Naga.png" alt="Naga" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Проверка скилов под данный корабль</p>
            </div>
            <div class="box">
                <img src="/resources/img/Hurricane fleet.png" alt="Hurricane fleet" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Проверка скилов под данный корабль</p>
            </div>
            <div class="box">
                <img src="/resources/img/Ishtar.png" alt="Ishtar" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Проверка скилов под данный корабль</p>
            </div>
            <div class="box">
                <img src="/resources/img/Rattlesnake.png" alt="Rattlesnake" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Проверка скилов под данный корабль</p>
            </div>
            <div class="box">
                <img src="/resources/img/Nightmare.png" alt="Nightmare" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Проверка скилов под данный корабль</p>
            </div>
            <div class="box">
                <img src="/resources/img/Machariel.png" alt="Machariel" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Проверка скилов под данный корабль</p>
            </div>
        </div>
    </div>
</div>
</body>
</html>