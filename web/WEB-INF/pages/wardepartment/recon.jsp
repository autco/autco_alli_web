<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Сайт альянса AUTISM COLLECTORS из игры EvE online">
    <meta name="Keywords" content="eve online, EVE, ева онлайн,">
    <title>AUTISM COLLECTORS</title>
    <%@include file="../template/header.jsp" %>
    <script type="text/javascript" src="/resources/js/fittings.js"></script>
    <script>
        $(document).ready(function(){
            $("ul.drop-0").toggle();
            $("ul.drop-1").toggle();
            var pageId = window.location.pathname.substring(0, window.location.pathname.lastIndexOf('.'));
            $('a[href*="' + pageId + '"]').addClass('active');
        });
    </script>
</head>
<body>
<div id="grid" class="grid">
    <div class="grid__item">
        <%@include file="../sidebar_left.jsp" %>
    </div>
    <div class="grid__item">
        <div class="content">
            <div class="logo"><img src="/img/logo.jpg" width="200" height="200" alt="logo">AUTISM COLLECTORS(тут типа на всю строку картинкой будет логотип вместе с надписью, которая прям в картинке будет прописана)</div>
            <div class="zag"><h1>low-skill</h1></div>
            <div class="box">
                <img src="/resources/img/Keres.jpg" alt="Keres" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Kitsune.jpg" alt="Kitsune" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Hyena.jpg" alt="Hyena" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Sentinel.jpg" alt="Sentinel" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="zag"><h1>mid-skill</h1></div>
            <div class="box">
                <img src="/resources/img/Lachesis.jpg" alt="Lachesis" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Rook.jpg" alt="Rook" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Huginn.jpg" alt="Huginn" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="box">
                <img src="/resources/img/Curse.jpg" alt="Curse" width="100%">
                <p>Фит 1</p>
                <p>Фит 2</p>
                <p>Фит 3</p>
                <p>Рекомендуемые импланты</p>
                <p>Рекомендуемые бустеры</p>
            </div>
            <div class="zag"><h1>high-skill</h1></div>

        </div>    
    </div>
</div>
</body>
</html>