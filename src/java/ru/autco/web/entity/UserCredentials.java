/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.autco.web.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import ru.autco.web.common.ERole;
import ru.autco.web.helpers.RoleHelper;
import ru.autco.web.helpers.UsersHelper;

/**
 *
 * @author t_grid
 */
@Entity
@Table(name = "user_credentials", catalog = "autco_db", schema = "")
@NamedQueries({
    @NamedQuery(name = "UserCredentials.findAll", query = "SELECT u FROM UserCredentials u")
    , @NamedQuery(name = "UserCredentials.findById", query = "SELECT u FROM UserCredentials u WHERE u.id = :id")
    , @NamedQuery(name = "UserCredentials.findByEmail", query = "SELECT u FROM UserCredentials u WHERE u.email = :email")
    , @NamedQuery(name = "UserCredentials.findByPassword", query = "SELECT u FROM UserCredentials u WHERE u.password = :password")
    , @NamedQuery(name = "UserCredentials.findByApproved", query = "SELECT u FROM UserCredentials u WHERE u.approved = :approved")})
public class UserCredentials implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userid", fetch = FetchType.EAGER)
    private Collection<UserLog> userLogCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userid", fetch = FetchType.EAGER)
    private Collection<EventSubscribers> eventSubscribersCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "email", nullable = false, length = 45)
    private String email;
    @Basic(optional = false)
    @Column(name = "password", nullable = false, length = 45)
    private String password;
    @Transient
    private String password2;
    @Basic(optional = false)
    @Column(name = "approved", nullable = false)
    private int approved;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userid", fetch = FetchType.EAGER)
    private Collection<RoleSiteadmin> roleSiteadminCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userid", fetch = FetchType.EAGER)
    private Collection<RoleCeo> roleCeoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userid", fetch = FetchType.EAGER)
    private Collection<RoleFleetcommander> roleFleetcommanderCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userid", fetch = FetchType.EAGER)
    private Collection<RoleOfficer> roleOfficerCollection;

    public UserCredentials() {
    }

    public UserCredentials(Integer id) {
        this.id = id;
    }

    public UserCredentials(Integer id, String email, String password, int approved) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.approved = approved;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getApproved() {
        return approved;
    }

    public void setApproved(int approved) {
        this.approved = approved;
    }

    public Collection<RoleSiteadmin> getRoleSiteadminCollection() {
        return roleSiteadminCollection;
    }

    public void setRoleSiteadminCollection(Collection<RoleSiteadmin> roleSiteadminCollection) {
        this.roleSiteadminCollection = roleSiteadminCollection;
    }

    public Collection<RoleCeo> getRoleCeoCollection() {
        return roleCeoCollection;
    }

    public void setRoleCeoCollection(Collection<RoleCeo> roleCeoCollection) {
        this.roleCeoCollection = roleCeoCollection;
    }

    public Collection<RoleFleetcommander> getRoleFleetcommanderCollection() {
        return roleFleetcommanderCollection;
    }

    public void setRoleFleetcommanderCollection(Collection<RoleFleetcommander> roleFleetcommanderCollection) {
        this.roleFleetcommanderCollection = roleFleetcommanderCollection;
    }

    public Collection<RoleOfficer> getRoleOfficerCollection() {
        return roleOfficerCollection;
    }

    public void setRoleOfficerCollection(Collection<RoleOfficer> roleOfficerCollection) {
        this.roleOfficerCollection = roleOfficerCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserCredentials)) {
            return false;
        }
        UserCredentials other = (UserCredentials) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.auto.web.entity.UserCredentials[ id=" + id + " ]";
    }
    
    public boolean haveSiteadminRole() {
        return RoleHelper.haveRole(this, ERole.RoleSiteAdmin);
    }
    
    public boolean haveCEORole() {
        return RoleHelper.haveRole(this, ERole.RoleCeo);
    }
    
    public boolean haveFleetCommanderRole() {
        return RoleHelper.haveRole(this, ERole.RoleFleetcommander);
    }
    
    public boolean haveOfficerRole() {
        return RoleHelper.haveRole(this, ERole.RoleOfficer);
    }
    
    public boolean haveSubscriptionToEvent(int id) {
        return UsersHelper.checkSubToEvt(id, this);
    }

    /**
     * @return the password2
     */
    public String getPassword2() {
        return password2;
    }

    /**
     * @param password2 the password2 to set
     */
    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public Collection<EventSubscribers> getEventSubscribersCollection() {
        return eventSubscribersCollection;
    }

    public void setEventSubscribersCollection(Collection<EventSubscribers> eventSubscribersCollection) {
        this.eventSubscribersCollection = eventSubscribersCollection;
    }

    public Collection<UserLog> getUserLogCollection() {
        return userLogCollection;
    }

    public void setUserLogCollection(Collection<UserLog> userLogCollection) {
        this.userLogCollection = userLogCollection;
    }
    
}
