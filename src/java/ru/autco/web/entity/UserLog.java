/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.autco.web.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author t_grid
 */
@Entity
@Table(name = "user_log", catalog = "autco_db", schema = "")
@NamedQueries({
    @NamedQuery(name = "UserLog.findAll", query = "SELECT u FROM UserLog u")
    , @NamedQuery(name = "UserLog.findById", query = "SELECT u FROM UserLog u WHERE u.id = :id")
    , @NamedQuery(name = "UserLog.findByLogaction", query = "SELECT u FROM UserLog u WHERE u.logaction = :logaction")
    , @NamedQuery(name = "UserLog.findByLogtime", query = "SELECT u FROM UserLog u WHERE u.logtime = :logtime")})
public class UserLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "logaction", nullable = false, length = 45)
    private String logaction;
    @Basic(optional = false)
    @Column(name = "logtime", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date logtime;
    @JoinColumn(name = "userid", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private UserCredentials userid;

    public UserLog() {
    }

    public UserLog(Integer id) {
        this.id = id;
    }

    public UserLog(Integer id, String logaction, Date logtime) {
        this.id = id;
        this.logaction = logaction;
        this.logtime = logtime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogaction() {
        return logaction;
    }

    public void setLogaction(String logaction) {
        this.logaction = logaction;
    }

    public Date getLogtime() {
        return logtime;
    }

    public void setLogtime(Date logtime) {
        this.logtime = logtime;
    }

    public UserCredentials getUserid() {
        return userid;
    }

    public void setUserid(UserCredentials userid) {
        this.userid = userid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserLog)) {
            return false;
        }
        UserLog other = (UserLog) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.auto.web.entity.UserLog[ id=" + id + " ]";
    }
    
}
