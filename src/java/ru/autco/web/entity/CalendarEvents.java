/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.autco.web.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author t_grid
 */
@Entity
@Table(name = "calendar_events", catalog = "autco_db", schema = "")
@NamedQueries({
    @NamedQuery(name = "CalendarEvents.findAll", query = "SELECT c FROM CalendarEvents c")
    , @NamedQuery(name = "CalendarEvents.findById", query = "SELECT c FROM CalendarEvents c WHERE c.id = :id")
    , @NamedQuery(name = "CalendarEvents.findByEventdate", query = "SELECT c FROM CalendarEvents c WHERE c.eventdate = :eventdate")
    , @NamedQuery(name = "CalendarEvents.findByTitle", query = "SELECT c FROM CalendarEvents c WHERE c.title = :title")
    , @NamedQuery(name = "CalendarEvents.findByDescription", query = "SELECT c FROM CalendarEvents c WHERE c.description = :description")})
public class CalendarEvents implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "eventdate", nullable = false)
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date eventdate;
    @Basic(optional = false)
    @Column(name = "title", nullable = false, length = 20)
    private String title;
    @Basic(optional = false)
    @Column(name = "description", nullable = false, length = 60)
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evtid", fetch = FetchType.EAGER)
    private Collection<EventSubscribers> eventSubscribersCollection;

    public CalendarEvents() {
    }

    public CalendarEvents(Integer id) {
        this.id = id;
    }

    public CalendarEvents(Integer id, Date eventdate, String title, String description) {
        this.id = id;
        this.eventdate = eventdate;
        this.title = title;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getEventdate() {
        return eventdate;
    }

    public void setEventdate(Date eventdate) {
        this.eventdate = eventdate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<EventSubscribers> getEventSubscribersCollection() {
        return eventSubscribersCollection;
    }

    public void setEventSubscribersCollection(Collection<EventSubscribers> eventSubscribersCollection) {
        this.eventSubscribersCollection = eventSubscribersCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CalendarEvents)) {
            return false;
        }
        CalendarEvents other = (CalendarEvents) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.auto.web.entity.CalendarEvents[ id=" + id + " ]";
    }
    
}
