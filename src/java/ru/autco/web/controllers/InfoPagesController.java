package ru.autco.web.controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import ru.autco.web.beans.UserSSO;

@Controller
@SessionAttributes("userSSO")
public class InfoPagesController {
    
    private boolean hasUserLoggedIn(UserSSO sso) {
        return sso.hasUserCred();
    }
    
    @RequestMapping(value = "/wardepartment/atm", method = RequestMethod.GET)
    public String showAtm(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso)) {
            model.addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return "wardepartment/atm";
        }
        return "redirect:/register";
    }
    @RequestMapping(value = "/wardepartment/ceptor", method = RequestMethod.GET)
    public String showCeptor(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso)) {
            model.addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return "wardepartment/ceptor";
        }
        return "redirect:/register";
    }
    @RequestMapping(value = "/wardepartment/command", method = RequestMethod.GET)
    public String showCommand(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso)) {
            model.addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return "wardepartment/command";
        }
        return "redirect:/register";
    }
    @RequestMapping(value = "/contacts", method = RequestMethod.GET)
    public String showContacts(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        //if (hasUserLoggedIn(sso))
            return "contacts";
        //return "redirect:/register";
    }
    @RequestMapping(value = "/wardepartment/dictor", method = RequestMethod.GET)
    public String showDictor(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso)) {
            model.addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return "wardepartment/dictor";
        }
        return "redirect:/register";
    }
    @RequestMapping(value = "/wardepartment/dps", method = RequestMethod.GET)
    public String showDPS(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso)) {
            model.addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return "wardepartment/dps";
        }
        return "redirect:/register";
    }
    @RequestMapping(value = "/wardepartment/logi", method = RequestMethod.GET)
    public String showLogi(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso)) {
            model.addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return "wardepartment/logi";
        }
        return "redirect:/register";
    }
    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String showNews(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso)) {
            model.addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return "news";
        }
        return "redirect:/register";
    }
    @RequestMapping(value = "/wardepartment/recon", method = RequestMethod.GET)
    public String showRecon(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso)) {
            model.addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return "wardepartment/recon";
        }
        return "redirect:/register";
    }
    @RequestMapping(value = "/regular", method = RequestMethod.GET)
    public String showRegular(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso)) {
            model.addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return "regular";
        }
        return "redirect:/register";
    }
    
    @ModelAttribute("userSSO")
    public UserSSO createUserSSO() {
        return new UserSSO();
    }
}
