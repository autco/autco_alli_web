/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.autco.web.controllers;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ru.autco.web.beans.UserSSO;
import ru.autco.web.common.ERole;
import ru.autco.web.helpers.UserLogHelper;
import ru.autco.web.helpers.UsersHelper;
import ru.autco.web.entity.UserCredentials;
import ru.autco.web.entity.UserLog;

/**
 *
 * @author t_grid
 */
@Controller
@SessionAttributes("userSSO")
@RequestMapping(value = "/admin")
public class AdminPagesController {
    
    @ModelAttribute("userSSO")
    public UserSSO createUserSSO() {
        return new UserSSO();
    }
    
    @ModelAttribute("searchUserCred")
    public UserCredentials createSearchUser() {
        return new UserCredentials();
    }
    
    private boolean hasUserLoggedIn(UserSSO sso) {
        return sso.hasUserCred();
    }
    
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ModelAndView showUsers(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso) && sso.getUserCred().haveSiteadminRole()) {
            List<UserCredentials> usersList = UsersHelper.list();
            ModelAndView mav = new ModelAndView("/admin/users", "usersList", usersList);
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/register"));
    }
    
    @RequestMapping(value = "/users/deleteUser/{id}", method = RequestMethod.GET)
    public ModelAndView deleteUser(ModelMap model, @PathVariable("id") int id, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso) && sso.getUserCred().haveSiteadminRole() && sso.getUserCred().getId() != id) {
            UsersHelper.delete(id);
            ModelAndView mav = new ModelAndView(new RedirectView("/admin/users"));
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/register"));
    }
    
    @RequestMapping(value = "/users/approveUser/{id}", method = RequestMethod.GET)
    public ModelAndView approveUser(ModelMap model, @PathVariable("id") int id, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso) && sso.getUserCred().haveSiteadminRole()) {
            UsersHelper.approve(id);
            ModelAndView mav = new ModelAndView(new RedirectView("/admin/users"));
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/register"));
    }
    
    @RequestMapping(value = "/users/userSearch", method = RequestMethod.POST)
    public ModelAndView searchUser(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, @ModelAttribute("searchUserCred") UserCredentials usr,
            HttpServletRequest request) {
        if (hasUserLoggedIn(sso) && sso.getUserCred().haveSiteadminRole()) {
            usr = UsersHelper.getByEmail(usr.getEmail());
            if (usr != null)
                return new ModelAndView(new RedirectView("/profile/" + usr.getId()));
            ModelAndView mav = new ModelAndView(new RedirectView("/admin/users"));
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/register"));
    }
    
    @RequestMapping(value = "/groups", method = RequestMethod.GET)
    public ModelAndView showGroups(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso) && sso.getUserCred().haveSiteadminRole()) {
            ModelAndView mav = new ModelAndView("/admin/groups");
            List<UserCredentials> adminsList = UsersHelper.listByRole(ERole.RoleSiteAdmin);
            List<UserCredentials> ceosList = UsersHelper.listByRole(ERole.RoleCeo);
            List<UserCredentials> offcsList = UsersHelper.listByRole(ERole.RoleOfficer);
            List<UserCredentials> fcsList = UsersHelper.listByRole(ERole.RoleFleetcommander);
            mav.getModelMap().addAttribute("adminsList", adminsList);
            mav.getModelMap().addAttribute("ceosList", ceosList);
            mav.getModelMap().addAttribute("offcsList", offcsList);
            mav.getModelMap().addAttribute("fcsList", fcsList);
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/register"));
    }
    
    @RequestMapping(value = "/groups/removeFromSiteadmins/{id}", method = RequestMethod.GET)
    public ModelAndView removeFromSiteadmins(ModelMap model, @PathVariable("id") int id, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso) && sso.getUserCred().haveSiteadminRole() && sso.getUserCred().getId() != id) {
            UsersHelper.dropFromGroup(ERole.RoleSiteAdmin, id);
            ModelAndView mav = new ModelAndView(new RedirectView("/admin/groups"));
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/register"));
    }
    
    @RequestMapping(value = "/groups/removeFromCEOs/{id}", method = RequestMethod.GET)
    public ModelAndView removeFromCEOs(ModelMap model, @PathVariable("id") int id, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso) && sso.getUserCred().haveSiteadminRole() && sso.getUserCred().getId() != id) {
            UsersHelper.dropFromGroup(ERole.RoleCeo, id);
            ModelAndView mav = new ModelAndView(new RedirectView("/admin/groups"));
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/register"));
    }
    
    @RequestMapping(value = "/groups/removeFromOfficers/{id}", method = RequestMethod.GET)
    public ModelAndView removeFromOfficers(ModelMap model, @PathVariable("id") int id, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso) && sso.getUserCred().haveSiteadminRole() && sso.getUserCred().getId() != id) {
            UsersHelper.dropFromGroup(ERole.RoleOfficer, id);
            ModelAndView mav = new ModelAndView(new RedirectView("/admin/groups"));
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/register"));
    }
    
    @RequestMapping(value = "/groups/removeFromFleetCommanders/{id}", method = RequestMethod.GET)
    public ModelAndView removeFromFleetCommanders(ModelMap model, @PathVariable("id") int id, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso) && sso.getUserCred().haveSiteadminRole() && sso.getUserCred().getId() != id) {
            UsersHelper.dropFromGroup(ERole.RoleFleetcommander, id);
            ModelAndView mav = new ModelAndView(new RedirectView("/admin/groups"));
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/register"));
    }
    
    @RequestMapping(value = "/log", method = RequestMethod.GET)
    public ModelAndView showLogs(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (hasUserLoggedIn(sso) && sso.getUserCred().haveSiteadminRole()) {
            ModelAndView mav = new ModelAndView("/admin/log");
            List<UserLog> logsList = UserLogHelper.list();
            mav.getModelMap().addAttribute("logsList", logsList);
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/register"));
    }
}
