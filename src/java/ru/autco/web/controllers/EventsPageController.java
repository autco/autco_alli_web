/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.autco.web.controllers;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ru.autco.web.beans.UserSSO;
import ru.autco.web.helpers.CalendarEventsHelper;
import ru.autco.web.helpers.UsersHelper;
import ru.autco.web.entity.CalendarEvents;
import ru.autco.web.entity.UserCredentials;

/**
 *
 * @author t_grid
 */
@Controller
@SessionAttributes("userSSO")
public class EventsPageController {
    
    @ModelAttribute("userSSO")
    public UserSSO addUserSSO() {
            UserSSO bean = new UserSSO();
            return bean;
    }
    
    @RequestMapping(value = "/events", method = RequestMethod.GET)
    public String showEventsPage(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, @ModelAttribute("calendarEvent") CalendarEvents evt, HttpServletRequest request) {
        if (sso.hasUserCred()) {
            Boolean isEventAdding = (Boolean) model.get("isEventAdding");
            if (isEventAdding == null) {
                isEventAdding = false;
                model.addAttribute("isEventAdding", isEventAdding);
            }
            if (!isEventAdding) {
                List<CalendarEvents> evts = CalendarEventsHelper.list();
                String dsort = request.getParameter("sort");
                if (dsort != null) {
                    if (dsort.equals("dateDesc")) {
                        dsort = "dateAsc";
                        evts.sort((CalendarEvents o1, CalendarEvents o2) -> {
                            if (o1.getEventdate().after(o2.getEventdate()))
                                return 1;
                            return -1;   
                        });
                    }
                    else if (dsort.equals("dateAsc")) {
                        dsort = "dateDesc";
                        evts.sort((CalendarEvents o1, CalendarEvents o2) -> {
                            if (o1.getEventdate().after(o2.getEventdate()))
                                return -1;
                            return 1;
                        });  
                    }
                } else {
                    dsort = "dateDesc";
                }
                model.addAttribute("dateSort", dsort);
                model.addAttribute("evtList", evts);
                model.addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            }
            return "events";
        }
        return "/index";
    }
    
    @RequestMapping(value = "/events/addEvent", method = RequestMethod.GET)
    public ModelAndView addEvent(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        if (sso.hasUserCred() && sso.getUserCred().haveCEORole()) {
            ModelAndView mav = new ModelAndView("/events");
            mav.addObject("isEventAdding", true);
            CalendarEvents evt = new CalendarEvents();
            evt.setEventdate(new Date());
            mav.addObject("calendarEvent", evt);
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/index"));
    }

    @RequestMapping(value = "/events/confirmAdding", method = RequestMethod.POST)
    public ModelAndView confirmAdding(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, @ModelAttribute("calendarEvent") CalendarEvents evt, HttpServletRequest request) {
        if (sso.hasUserCred() && sso.getUserCred().haveCEORole()) {
            ModelAndView mav = new ModelAndView(new RedirectView("/events"));
            mav.addObject("isEventAdding", false);
            CalendarEventsHelper.add(evt, sso.getUserCred());
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/index"));
    }
    
    @RequestMapping(value = "/events/subscribe/{id}", method = RequestMethod.GET)
    public ModelAndView subscribeToEvent(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, @PathVariable("id") int id, HttpServletRequest request) {
        if (sso.hasUserCred()) {
            CalendarEventsHelper.subscribe(id, sso.getUserCred());
            ModelAndView mav = new ModelAndView(new RedirectView("/events"));
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/index"));
    }
    
    @RequestMapping(value = "/events/unsubscribe/{id}", method = RequestMethod.GET)
    public ModelAndView unSubscribeToEvent(ModelMap model, @ModelAttribute("userSSO") UserSSO sso, @PathVariable("id") int id, HttpServletRequest request) {
        if (sso.hasUserCred()) {
            CalendarEventsHelper.unSubscribe(id, sso.getUserCred());
            ModelAndView mav = new ModelAndView(new RedirectView("/events"));
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/index"));
    }
    
    @RequestMapping(value = "/eventinfo/{id}", method = RequestMethod.GET)
    public ModelAndView showEventById(ModelAndView mav, @ModelAttribute("userSSO") UserSSO sso, @PathVariable("id") int id, HttpServletRequest request) {
        if (sso.hasUserCred() && (sso.getUserCred().haveCEORole() || sso.getUserCred().haveSiteadminRole())) {
            mav = new ModelAndView("/eventinfo");
            CalendarEvents evt = CalendarEventsHelper.getById(id);
            List<UserCredentials> subsList = UsersHelper.listByEventId(id);
            mav.addObject("subsList", subsList);
            mav.addObject("evt", evt);
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            return mav;
        }
        return new ModelAndView(new RedirectView("/index"));
    }
}
