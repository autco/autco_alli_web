/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.autco.web.controllers;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ru.autco.web.beans.UserSSO;
import ru.autco.web.common.ERole;
import ru.autco.web.helpers.RoleHelper;
import ru.autco.web.helpers.UsersHelper;
import ru.autco.web.entity.UserCredentials;

/**
 *
 * @author t_grid
 */
@Controller
@SessionAttributes("userSSO")
public class ProfilePageController {
    
    @ModelAttribute("userSSO")
    public UserSSO createUserSSO() {
        return new UserSSO();
    }
    
    @RequestMapping(value = "/profile/{id}", method = RequestMethod.GET)
    public ModelAndView showProfile(ModelMap model, @PathVariable("id") int id, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView(new RedirectView("/register")); 
        if (sso.hasUserCred() && (sso.getUserCred().haveSiteadminRole() || sso.getUserCred().haveCEORole() || sso.getUserCred().getId() == id)) {
            UserCredentials u = UsersHelper.getById(id);
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            if (u != null) {
                mav = new ModelAndView("/profile");
                mav.addObject("userCred", u);
            }
        }
        return mav;
    }
    
    @RequestMapping(value = "/profile/addToSiteadmins/{id}", method = RequestMethod.GET)
    public ModelAndView addToSiteadmins(ModelMap model, @PathVariable("id") int id, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView(new RedirectView("/register")); 
        if (sso.hasUserCred() && (sso.getUserCred().haveSiteadminRole() || sso.getUserCred().haveCEORole())) {
            mav = new ModelAndView(new RedirectView("/profile/" + id));
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            RoleHelper.addToRole(id, ERole.RoleSiteAdmin);
        }
        return mav;
    }
    @RequestMapping(value = "/profile/addToFleetCommanders/{id}", method = RequestMethod.GET)
    public ModelAndView addToFleetCommanders(ModelMap model, @PathVariable("id") int id, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView(new RedirectView("/register")); 
        if (sso.hasUserCred() && (sso.getUserCred().haveSiteadminRole() || sso.getUserCred().haveCEORole())) {
            mav = new ModelAndView(new RedirectView("/profile/" + id));
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            RoleHelper.addToRole(id, ERole.RoleFleetcommander);
        }
        return mav;
    }
    @RequestMapping(value = "/profile/addToCeos/{id}", method = RequestMethod.GET)
    public ModelAndView addToCeos(ModelMap model, @PathVariable("id") int id, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView(new RedirectView("/register")); 
        if (sso.hasUserCred() && (sso.getUserCred().haveSiteadminRole() || sso.getUserCred().haveCEORole())) {
            mav = new ModelAndView(new RedirectView("/profile/" + id));
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            RoleHelper.addToRole(id, ERole.RoleCeo);
        }
        return mav;
    }
    @RequestMapping(value = "/profile/addToOfficers/{id}", method = RequestMethod.GET)
    public ModelAndView addToOfficers(ModelMap model, @PathVariable("id") int id, @ModelAttribute("userSSO") UserSSO sso, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView(new RedirectView("/register")); 
        if (sso.hasUserCred() && (sso.getUserCred().haveSiteadminRole() || sso.getUserCred().haveCEORole())) {
            mav = new ModelAndView(new RedirectView("/profile/" + id));
            mav.getModelMap().addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
            RoleHelper.addToRole(id, ERole.RoleOfficer);
        }
        return mav;
    }
}
