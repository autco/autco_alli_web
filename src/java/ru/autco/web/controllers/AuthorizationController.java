package ru.autco.web.controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ru.autco.web.beans.UserSSO;
import ru.autco.web.helpers.LoginHelper;
import ru.autco.web.helpers.RegHelper;
import ru.autco.web.helpers.UserLogHelper;
import ru.autco.web.entity.UserCredentials;
import ru.autco.web.entity.UserLog;

@Controller
@SessionAttributes("userSSO")
public class AuthorizationController {
    private final static String EVE_APP_URL = "https://login.eveonline.com/oauth/authorize/"
            + "?response_type=code"
            + "&redirect_uri=http://localhost:8084/index.htm"
            + "&client_id=342b7b957c5d4905a59bf9d4c309bc5a"
            + "&scope=characterStatsRead%20characterSkillsRead";

    @ModelAttribute("userSSO")
    public UserSSO addUserSSO() {
            UserSSO bean = new UserSSO();
            return bean;
    }
    
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String showMainPage(ModelMap model, HttpServletRequest request) {
        UserSSO sso = (UserSSO) model.get("userSSO");
        String code = "";
        if (sso.hasCode())
            code = sso.getStoredCode();
        else
            code = request.getParameter("code");
        if (code == null || code.equals(""))
            code = "No code!";
        else {
            sso.setStoredCode(code);
        }
        model.addAttribute("message", code);
        if (sso.hasUserCred())
            model.addAttribute("hasSiteAdminRole", sso.getUserCred().haveSiteadminRole());
      
        return "index";
    }
   
    @RequestMapping(value = "/index", method = RequestMethod.GET, params="authorize")
    public String authorize(HttpServletRequest request) {
        return "redirect:" + EVE_APP_URL;
    }
    
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView showRegister(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("register");
        mav.addObject("authUserCred", new UserCredentials());
        mav.addObject("regUserCred", new UserCredentials());
        String erc = request.getParameter("regErrorCode");
        if (erc != null) {
            int x = Integer.parseInt(erc);
            switch(x) {
                case 1:
                    erc = "Пароли не совпадают!";
                    break;
                case 2:
                    erc = "Не удалось зарегистрироваться!";
                    break;
                default:
                    erc = "";
                    break;
            }
            mav.addObject("regErrorMsg", erc);
        }
        return mav;
    }

    @RequestMapping(value = "/processAuthorize", method = RequestMethod.POST)
    public ModelAndView validateUser(HttpServletRequest request, HttpServletResponse response, ModelAndView mav,
                                @ModelAttribute("authUserCred") UserCredentials user) {
        user = LoginHelper.authorize(user);
        if (user != null) {
            UserSSO sso = (UserSSO) mav.getModelMap().get("userSSO");
            mav = new ModelAndView(new RedirectView("index"));
            if (sso == null)
                sso = new UserSSO();
            sso.setUserCred(user);
            mav.getModelMap().addAttribute("userSSO", sso);
        } else {
            mav.setView(new RedirectView("register"));
        }
        return mav;
    }
    
    @RequestMapping(value = "/processRegister", method = RequestMethod.POST)
    public ModelAndView registerUser(HttpServletRequest request, HttpServletResponse response, ModelAndView mav,
                                @ModelAttribute("regUserCred") UserCredentials user) {
        if (!user.getPassword().equals(user.getPassword2())) {
            mav.setView(new RedirectView("register"));
            mav.getModelMap().addAttribute("regErrorCode", 1);
            mav.getModelMap().addAttribute("regUserCred", user);
        }
        else {
            if (RegHelper.reg(user)) {
                mav = new ModelAndView(new RedirectView("index"));
            } else {
                mav.setView(new RedirectView("register"));
                mav.getModelMap().addAttribute("regErrorCode", 2);
            }
        }
        return mav;
    }
    
    @RequestMapping(value = "/processLogout", method = RequestMethod.GET)
    public ModelAndView processLogout(HttpServletRequest request, HttpServletResponse response, ModelAndView mav, SessionStatus status,
            @ModelAttribute("userSSO") UserSSO sso) {
        mav.setView(new RedirectView("/index"));
        if (sso.hasUserCred()) {
            UserLogHelper.log(sso.getUserCred(), "Вышел");
            status.setComplete();
        }
        return mav;
    }

}
