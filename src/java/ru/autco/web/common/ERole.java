/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.autco.web.common;

/**
 *
 * @author t_grid
 */
public enum ERole {
    RoleCeo,
    RoleFleetcommander,
    RoleOfficer,
    RoleSiteAdmin;
    
    @Override
    public String toString() {
        String str;
        switch(this) {
            case RoleCeo:
                str = "RoleCeo";
                break;
            case RoleFleetcommander:
                str = "RoleFleetcommander";
                break;
            case RoleOfficer:
                str = "RoleOfficer";
                break;
            case RoleSiteAdmin:
                str = "RoleSiteadmin";
                break;
            default:
                str = "";
                break;
        }
        return str;
    }
}
