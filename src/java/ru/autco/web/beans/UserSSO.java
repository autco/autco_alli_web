package ru.autco.web.beans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.autco.web.entity.UserCredentials;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.


/**
 *
 * @author t_grid
 */
@Component
@Scope("session")
public class UserSSO {
    private String storedCode = "";
    private UserCredentials userCred;

    /**
     * @return the storedCode
     */
    public String getStoredCode() {
        return storedCode;
    }

    /**
     * @param storedCode the storedCode to set
     */
    public void setStoredCode(String storedCode) {
        this.storedCode = storedCode;
    }
    
    public boolean hasCode() {
        return !this.storedCode.equals("");
    }

    /**
     * @return the userCred
     */
    public UserCredentials getUserCred() {
        return userCred;
    }

    /**
     * @param userCred the userCred to set
     */
    public void setUserCred(UserCredentials userCred) {
        this.userCred = userCred;
    }
    
    public boolean hasUserCred() {
        return this.userCred != null;
    }
}
