/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.autco.web.helpers;


import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import ru.autco.web.entity.UserCredentials;

/**
 *
 * @author t_grid
 */
public class LoginHelper implements Serializable{
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
        .createEntityManagerFactory("autco_alli_webPU");
    
    public static UserCredentials authorize(UserCredentials cred) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        List<UserCredentials> users = null;
        tx.begin();
        try {
            CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<UserCredentials> c = qb.createQuery(UserCredentials.class);
            Root<UserCredentials> p = c.from(UserCredentials.class);
            Predicate condition = qb.and(qb.equal(p.get("email"), cred.getEmail()), qb.equal(p.get("password"), cred.getPassword()));
            c.where(condition);
            TypedQuery<UserCredentials> q = em.createQuery(c);
            users = q.getResultList();
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        if (users != null && users.size() == 1) {
            UserLogHelper.log(users.get(0), "Авторизовался");
            return users.get(0);
        }
        return null;
    }
}
