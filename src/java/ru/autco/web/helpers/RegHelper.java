/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.autco.web.helpers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import ru.autco.web.entity.UserCredentials;

/**
 *
 * @author t_grid
 */
public class RegHelper {
    
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory("autco_alli_webPU");

    public static boolean reg(UserCredentials user) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        boolean ret = false;
        tx.begin();
        try {
            Query q = em.createQuery("from UserCredentials u where u.email = :email and u.password = :password", UserCredentials.class);
            q.setParameter("email", user.getEmail());
            q.setParameter("password", user.getPassword());
            if (q.getResultList().isEmpty()) {
                em.persist(user);
                ret = true;
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        if (ret)
            UserLogHelper.log(user, "Зарегистрировался");
        return ret;
    }
    
}
