/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.autco.web.helpers;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import ru.autco.web.entity.CalendarEvents;
import ru.autco.web.entity.EventSubscribers;
import ru.autco.web.entity.RoleSiteadmin;
import ru.autco.web.entity.UserCredentials;

/**
 *
 * @author t_grid
 */
public class CalendarEventsHelper {
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
        .createEntityManagerFactory("autco_alli_webPU");
    
    public static boolean add(CalendarEvents evt, UserCredentials usr) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        boolean ret = false;
        tx.begin();
        try {
            em.persist(evt);
            ret = true;
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }  
        em.close();
        if (ret)
            UserLogHelper.log(usr, "Создал событие " + evt.getId());
        return ret;
    }
    
    public static List<CalendarEvents> list() {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        List<CalendarEvents> evts = null;
        tx.begin();
        try {
            CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<CalendarEvents> c = qb.createQuery(CalendarEvents.class);
            Root<CalendarEvents> p = c.from(CalendarEvents.class);
            TypedQuery<CalendarEvents> q = em.createQuery(c);
            evts = q.getResultList();
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        return evts;
    }
    
    public static CalendarEvents getById(int id) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        CalendarEvents evt = null;
        tx.begin();
        try {
            CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<CalendarEvents> c = qb.createQuery(CalendarEvents.class);
            Root<CalendarEvents> p = c.from(CalendarEvents.class);
            Predicate condition = qb.equal(p.get("id"), id);
            c.where(condition);
            TypedQuery<CalendarEvents> q = em.createQuery(c);
            List<CalendarEvents> evts = q.getResultList();
            if (!evts.isEmpty())
                evt = evts.get(0);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        return evt;
    }
    
    public static boolean subscribe(int evtId, UserCredentials usr) {
       EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        CalendarEvents evt = getById(evtId);
        boolean ret = false;
        tx.begin();
        try {
            EventSubscribers es = new EventSubscribers();
            es.setEvtid(evt);
            es.setUserid(usr);
            em.persist(es);
            tx.commit();
            ret = true;
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        if (ret)
            UserLogHelper.log(usr, "Подписался на событие " + evtId);
        return ret;
    }
    
    public static boolean unSubscribe(int evtId, UserCredentials usr) {
       EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        CalendarEvents evt = getById(evtId);
        boolean ret = false;
        tx.begin();
        CriteriaBuilder qb = em.getCriteriaBuilder();
        try {
            CriteriaDelete delete = qb.createCriteriaDelete(EventSubscribers.class);
            Root rl = delete.from(EventSubscribers.class);
            delete.where(qb.and(qb.equal(rl.get("userid"), usr), qb.equal(rl.get("evtid"), evt)));
            Query q = em.createQuery(delete);
            q.executeUpdate();
            tx.commit();
            ret = true;
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        if (ret)
            UserLogHelper.log(usr, "Отписался от события " + evtId);
        return ret;
    }
}
