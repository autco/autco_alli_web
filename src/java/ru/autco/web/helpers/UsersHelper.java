/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.autco.web.helpers;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import ru.autco.web.common.ERole;
import ru.autco.web.entity.EventSubscribers;
import ru.autco.web.entity.RoleCeo;
import ru.autco.web.entity.RoleFleetcommander;
import ru.autco.web.entity.RoleOfficer;
import ru.autco.web.entity.RoleSiteadmin;
import ru.autco.web.entity.UserCredentials;

/**
 *
 * @author t_grid
 */
public class UsersHelper {
    
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory("autco_alli_webPU");
    
    public static List<UserCredentials> list() {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        List<UserCredentials> users = null;
        tx.begin();
        try {
            CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<UserCredentials> c = qb.createQuery(UserCredentials.class);
            Root<UserCredentials> p = c.from(UserCredentials.class);
            TypedQuery<UserCredentials> q = em.createQuery(c);
            users = q.getResultList();
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        return users;
    }
    
    public static List<UserCredentials> listByRole(ERole role) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        List<UserCredentials> users = null;
        tx.begin();
        try {
            if (role != null) {
                Query q = em.createQuery("SELECT u FROM UserCredentials u, " + role.toString() + " rs WHERE rs.userid.id = u.id", UserCredentials.class);
                if (q != null) {
                    users = q.getResultList();
                }
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        return users;
    }
    
    public static List<UserCredentials> listByEventId(int id) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        List<UserCredentials> users = null;
        tx.begin();
        try {
            Query q = em.createQuery("SELECT u FROM UserCredentials u, EventSubscribers es WHERE es.userid.id = u.id AND es.evtid.id = :id", UserCredentials.class);
            q.setParameter("id", id);
            users = q.getResultList();
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        return users;
    }
    
    public static UserCredentials getById(int id) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        UserCredentials user = null;
        tx.begin();
        try {
            Query q = em.createQuery("SELECT u FROM UserCredentials u WHERE u.id = :id", UserCredentials.class);
            q.setParameter("id", id);
            List<UserCredentials> u = q.getResultList();
            if (!u.isEmpty())
                user = u.get(0);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        return user;
    }
    
    public static UserCredentials getByEmail(String email) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        UserCredentials user = null;
        tx.begin();
        try {
            Query q = em.createQuery("SELECT u FROM UserCredentials u WHERE u.email = :email", UserCredentials.class);
            q.setParameter("email", email);
            List<UserCredentials> u = q.getResultList();
            if (!u.isEmpty())
                user = u.get(0);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        return user;
    }
    
    public static void delete(int id) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        UserCredentials usr = getById(id);
        tx.begin();
        boolean ret = false;
        try {
            Query q = em.createQuery("DELETE FROM UserCredentials u where u.id = :id");
            q.setParameter("id", id);
            q.executeUpdate();
            tx.commit();
            ret = true;
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        if (ret)
            UserLogHelper.log(usr, "Был удален");
    }
    
    public static void approve(int id) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        UserCredentials usr = getById(id);
        boolean ret = false;
        tx.begin();
        try {
            Query q = em.createQuery("UPDATE UserCredentials SET approved = 1 where id = :id");
            q.setParameter("id", id);
            q.executeUpdate();
            ret = true;
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        if (ret)
            UserLogHelper.log(usr, "Был подтвержден");
    }
    
    public static void dropFromGroup(ERole role, int id) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        UserCredentials usr = getById(id);
        boolean ret = false;
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        tx.begin();
        try {
            CriteriaDelete delete = null;
            Root rl = null;
            switch(role) {
                case RoleSiteAdmin:
                    delete = criteriaBuilder.createCriteriaDelete(RoleSiteadmin.class);
                    rl = delete.from(RoleSiteadmin.class);
                    break;
                case RoleCeo:
                    delete = criteriaBuilder.createCriteriaDelete(RoleCeo.class);
                    rl = delete.from(RoleCeo.class);
                    break;
                case RoleOfficer:
                    delete = criteriaBuilder.createCriteriaDelete(RoleOfficer.class);
                    rl = delete.from(RoleOfficer.class);
                    break;
                case RoleFleetcommander:
                    delete = criteriaBuilder.createCriteriaDelete(RoleFleetcommander.class);
                    rl = delete.from(RoleFleetcommander.class);
                    break;    
                default:
                    break;
            }
            if (delete != null && rl != null) {
                delete.where(criteriaBuilder.equal(rl.get("userid"), id));
                Query q = em.createQuery(delete);
                q.executeUpdate();
                ret = true;
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        if (ret)
            UserLogHelper.log(usr, "Был удален из группы " + role.toString());
    }
    
    public static boolean checkSubToEvt(int id, UserCredentials user) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        List<EventSubscribers> subs = null;
        tx.begin();
        CriteriaBuilder qb = em.getCriteriaBuilder();
        try {
            CriteriaQuery<EventSubscribers> c = qb.createQuery(EventSubscribers.class);
            Root<EventSubscribers> p = c.from(EventSubscribers.class);
            Predicate condition = qb.and(qb.equal(p.get("userid"), user), qb.equal(p.get("evtid"), CalendarEventsHelper.getById(id)));
            c.where(condition);
            TypedQuery<EventSubscribers> q = em.createQuery(c);
            subs = q.getResultList();
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        return subs != null && subs.size() == 1;
    }
}
