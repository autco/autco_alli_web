/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.autco.web.helpers;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import ru.autco.web.common.ERole;
import ru.autco.web.entity.RoleCeo;
import ru.autco.web.entity.RoleFleetcommander;
import ru.autco.web.entity.RoleOfficer;
import ru.autco.web.entity.RoleSiteadmin;
import ru.autco.web.entity.UserCredentials;

/**
 *
 * @author t_grid
 */
public class RoleHelper {
    
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
        .createEntityManagerFactory("autco_alli_webPU");
    
    public static List<RoleSiteadmin> listAdmins() {
        List<RoleSiteadmin> admins = null;
        
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<RoleSiteadmin> c = qb.createQuery(RoleSiteadmin.class);
            TypedQuery<RoleSiteadmin> q = em.createQuery(c);
            admins = q.getResultList();
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        return admins;
    }
    
    public static boolean haveRole(UserCredentials user, ERole role) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        Boolean ret = false;
        tx.begin();
        try {
            CriteriaBuilder qb = em.getCriteriaBuilder();
            switch(role) {
                case RoleSiteAdmin: {
                    List<RoleSiteadmin> roles = null;
                    CriteriaQuery<RoleSiteadmin> c = qb.createQuery(RoleSiteadmin.class);
                    Root<RoleSiteadmin> p = c.from(RoleSiteadmin.class);
                    Predicate condition = qb.equal(p.get("userid"), user);
                    c.where(condition);
                    TypedQuery<RoleSiteadmin> q = em.createQuery(c);
                    roles = q.getResultList();
                    ret = (roles != null && roles.size() == 1);
                    break;
                }
                case RoleCeo: {
                    List<RoleCeo> roles = null;
                    CriteriaQuery<RoleCeo> c = qb.createQuery(RoleCeo.class);
                    Root<RoleCeo> p = c.from(RoleCeo.class);
                    Predicate condition = qb.equal(p.get("userid"), user);
                    c.where(condition);
                    TypedQuery<RoleCeo> q = em.createQuery(c);
                    roles = q.getResultList();
                    ret = (roles != null && roles.size() == 1);
                    break;
                }
                case RoleFleetcommander: {
                    List<RoleFleetcommander> roles = null;
                    CriteriaQuery<RoleFleetcommander> c = qb.createQuery(RoleFleetcommander.class);
                    Root<RoleFleetcommander> p = c.from(RoleFleetcommander.class);
                    Predicate condition = qb.equal(p.get("userid"), user);
                    c.where(condition);
                    TypedQuery<RoleFleetcommander> q = em.createQuery(c);
                    roles = q.getResultList();
                    ret = (roles != null && roles.size() == 1);
                    break;
                }
                case RoleOfficer: {
                    List<RoleOfficer> roles = null;
                    CriteriaQuery<RoleOfficer> c = qb.createQuery(RoleOfficer.class);
                    Root<RoleOfficer> p = c.from(RoleOfficer.class);
                    Predicate condition = qb.equal(p.get("userid"), user);
                    c.where(condition);
                    TypedQuery<RoleOfficer> q = em.createQuery(c);
                    roles = q.getResultList();
                    ret = (roles != null && roles.size() == 1);
                    break;
                }
                default:
                    break;
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        return ret;
    }
    
    public boolean haveRoleSiteAdmin(UserCredentials user) {
        return haveRole(user, ERole.RoleSiteAdmin);
    }
    
    public static boolean addToRole(int id, ERole role) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        UserCredentials usr = UsersHelper.getById(id);
        boolean ret = false;
        tx.begin();
        try {
            switch(role) {
                case RoleCeo:
                    RoleCeo rc = new RoleCeo();
                    rc.setUserid(usr);
                    em.persist(rc);
                    ret = true;
                    break;
                case RoleFleetcommander:
                    RoleFleetcommander rf = new RoleFleetcommander();
                    rf.setUserid(usr);
                    em.persist(rf);
                    ret = true;
                    break;
                case RoleSiteAdmin:
                    RoleSiteadmin rs = new RoleSiteadmin();
                    rs.setUserid(usr);
                    em.persist(rs);
                    ret = true;
                    break;
                case RoleOfficer:
                    RoleOfficer ro = new RoleOfficer();
                    ro.setUserid(usr);
                    em.persist(ro);
                    ret = true;
                    break;
                default:
                    break;
                          
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        if (ret)
            UserLogHelper.log(usr, "Назначили роль " + role.toString());
        return ret;
    }
}
