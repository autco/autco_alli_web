/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.autco.web.helpers;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ru.autco.web.entity.UserCredentials;
import ru.autco.web.entity.UserLog;

/**
 *
 * @author t_grid
 */
public class UserLogHelper {
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory("autco_alli_webPU");

    public static boolean log(UserCredentials user, String action) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        boolean ret = false;
        tx.begin();
        try {
            UserLog log = new UserLog();
            log.setLogaction(action);
            log.setUserid(user);
            log.setLogtime(new Date());
            em.persist(log);
            ret = true;
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        return ret;
    }
    
    public static List<UserLog> list() {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        List<UserLog> logs = null;
        tx.begin();
        try {
            CriteriaBuilder qb = em.getCriteriaBuilder();
            CriteriaQuery<UserLog> c = qb.createQuery(UserLog.class);
            Root<UserLog> p = c.from(UserLog.class);
            TypedQuery<UserLog> q = em.createQuery(c);
            logs = q.getResultList();
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        return logs;
    }
}
